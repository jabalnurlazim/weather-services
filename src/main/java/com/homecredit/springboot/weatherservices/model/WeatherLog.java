package com.homecredit.springboot.weatherservices.model;

public class WeatherLog {
	private int id;
	private String responseId;
	private String location;
	private String actualWeather;
	private String temperature;
	private String dtimeInserted;


	public void setId(int id) {
		this.id = id;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getActualWeather() {
		return actualWeather;
	}

	public void setActualWeather(String actualWeather) {
		this.actualWeather = actualWeather;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public void setDtimeInserted(String dtimeInserted) {
		this.dtimeInserted = dtimeInserted;
	}
}
