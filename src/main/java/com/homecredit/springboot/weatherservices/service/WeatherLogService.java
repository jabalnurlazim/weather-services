package com.homecredit.springboot.weatherservices.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.homecredit.springboot.weatherservices.model.WeatherLog;
import java.sql.*;

@Component
public class WeatherLogService {
	private static List<WeatherLog> weatherLogList;
	
	private String apiUrl = "https://samples.openweathermap.org/data/2.5/group?id=2643743,4548393,5391959&units=metric&appid=d941de5dd02728061cfb48e63c0b23ef";
	private String myUrl = "jdbc:mysql://localhost/weatherlog";
    
	public List<WeatherLog> retrieveAllWeatherLogs() {
		try {
			weatherLogList = new ArrayList<>();
			URL url = new URL(apiUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			Connection connection = null;
		    PreparedStatement preparedStmt = null;

			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			
			try {
				Class.forName("com.mysql.jdbc.Driver"); 
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    try {
				connection = DriverManager.getConnection(myUrl, "root", "");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			ObjectMapper mapper = new ObjectMapper();
			String json = br.readLine();
			JsonNode rootNode = mapper.readTree(json);

		    String query = " insert into weatherlog (responseId, location, actualWeather, temperature)"
		            + " values (?, ?, ?, ?)";

			try {
				preparedStmt = connection.prepareStatement(query);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			int size = rootNode.get("list").size();
			for(int i=0; i < size; i++)
			{
				WeatherLog weather = new WeatherLog();
				String responseId = rootNode.get("list").get(i).get("id").toString();
				weather.setResponseId(responseId);
				weather.setLocation(rootNode.get("list").get(i).get("name").toString().replaceAll("\"", ""));
				weather.setActualWeather(rootNode.get("list").get(i).get("weather").get(0).get("main").toString().replaceAll("\"", ""));
				weather.setTemperature(rootNode.get("list").get(i).get("main").get("temp").toString());
				weather.setDtimeInserted(rootNode.get("list").get(i).get("dt").toString());

				// create the mysql insert preparedstatement
			    preparedStmt.setString (1, responseId);
		    	preparedStmt.setString (2, weather.getLocation());
		    	preparedStmt.setString (3, weather.getActualWeather());
		    	preparedStmt.setString (4, weather.getTemperature());
			    
				// execute the preparedstatement
				preparedStmt.addBatch();

				weatherLogList.add(weather);
			}

		    preparedStmt.executeBatch();
			conn.disconnect();

		  } catch (MalformedURLException e) {

			e.printStackTrace();

		  } catch (IOException e) {

			e.printStackTrace();

		  } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return weatherLogList;
	}
	
	public WeatherLog retrieveWeatherLog(String location) {
		for (WeatherLog weatherLog : weatherLogList) {
			if (weatherLog.getLocation().equalsIgnoreCase(location)) {
				return weatherLog;
			}
		}
		return null;
	}

	private Random  random = new Random ();
	public WeatherLog addWeatherLog(WeatherLog weatherLog) {

		int randomId = random.nextInt(50) + 1;
		
		weatherLog.setId(randomId);

		weatherLogList.add(weatherLog);

		return weatherLog;
	}
}
