package com.homecredit.springboot.weatherservices.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.homecredit.springboot.weatherservices.model.WeatherLog;
import com.homecredit.springboot.weatherservices.service.WeatherLogService;

@RestController
public class WeatherLogController {
	
	@Autowired
	private WeatherLogService weatherLogService;

	@GetMapping("/weatherlogs")
	public List<WeatherLog> retrieveWeatherLogs() {
		return weatherLogService.retrieveAllWeatherLogs();
	}
	
	@GetMapping("/weatherlog/{location}")
	public WeatherLog retrieveWeatherLog(@PathVariable String location) {
		return weatherLogService.retrieveWeatherLog(location);
	}
	
	@PostMapping("/weatherlog")
	public ResponseEntity<Void> registerStudentForCourse(@RequestBody WeatherLog newWeatherLog) {

		WeatherLog weatherLog = weatherLogService.addWeatherLog(newWeatherLog);

		if (weatherLog == null)
			return ResponseEntity.noContent().build();

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path(
				"/{id}").buildAndExpand(weatherLog.getLocation()).toUri();

		return ResponseEntity.created(location).build();
	}
}
